---
title: 'Formation & coaching'
date: 2018-11-28T15:14:54+10:00
featured: true
weight: 5
metaTitle: 'SEO référencement'
metaDescription: 'SEO meta description de fou'
metaCanonical: 'SEO'
metaOgImage: 'SEO'
metaOgTitle: 'SEO'
metaOgDescription: 'SEO'
metaOgUrl: 'SEO'
description: 'Apprenez à développer les synergies entre votre entreprise et le numérique.'
icon: 'services/icons8-source_code.png'
thumbnail: 'services/zany-jadraque-571205-unsplash.jpg'
heroTitleColor: "#bada55"
heroHeading: 'Web Development'
heroSubHeading: 'Référencement naturel (SEO), référencement payant, stratégie d’emailing, publicité, Community Management, etc... internet regorge de disciplines sur lesquelles nous pouvons vous former !'
heroBackground: 'services/zany-jadraque-571205-unsplash.jpg'
heroFullscreen: false
heroBackgroundOverlay: true
hero_background_gradient_one: '#000'
hero_background_gradient_two: 'red'
headerTransparent: true
heroDiagonal: true
heroHeight: 600
heroDiagonalFill: '#ffffff'
heroCtaPreText: "Besoin d'être coaché ?"
heroCtaText: 'Contactez-nous !'
heroCtaTextColor: ''
heroCtaLink: '#'
ctaFooterBg: '#bada55'
ctaFooterTitle: 'Envie de vous former sur le numérique ?'
ctaFooterPitch: "Rencontrons-nous dès maintenant pour discuter ensemble de votre projet !"
ctaFooterBtn: 'Contactez-nous !'
ctaFooterBtnColor: "red"
ctaFooterLink: '#'
---

Qweb.eco vous fait monter en compétence sur le numérique au travers de  formations, ateliers et coachings 100% sur mesure. 

Le web regroupe des disciplines complémentaires : développement web, marketing en ligne, webdesign, rédaction, UX design, analyse de données, etc... Se former au digital permet d’en comprendre les enjeux et de bien communiquer avec chacun de ces intervenants. Vous êtes alors mieux armé pour décider et plus inspiré pour développer votre entreprise avec internet.

{{% accordeon title="C'est fait  pour vous si vous souhaitez apprendre" %}}
- Comment gagner en visibilité et augmenter votre notoriété sur internet ?
- Comment bien gérer votre e-réputation ?
- Comment vendre plus avec internet ?
- Comment bien comprendre les données analytics et les utiliser de manière responsable ?
- Comment intégrer la dimension environnementale dans vos projets numériques ?
- Comment bien débuter sur Google Ads et faire performer vos campagnes d’acquisition ?
- Comment fonctionne le référencement local dans les moteurs de recherche ?
{{% /accordeon %}}

---
{{% text-center %}}
## Restez à la pointe du marketing digital
{{% /text-center%}}
Internet est un outil qui évolue très vite. Qweb.eco vous tient à jour sur tous les sujets abordés lors des formations et coachings. Afin de vous garantir un niveau d’expertise optimal, nous consacrons 25% de notre temps de travail à la veille marché et à l’expérimentation. Notre accompagnement sur le long terme et notre newsletter vous permettent de rester informé des nouveautés et de saisir immédiatement les opportunités en ligne.

---
{{% text-center %}}
## Optez pour une formation/coaching 100% contextualisée à votre entreprise, son secteur, son marché, ses besoins
{{% /text-center%}}
Nous concevons des modules de formation et de coaching 100% sur-mesure qui

- intègrent le contexte de votre entreprise
- répondent à vos objectifs de développement
- s’articulent autour de la pratique

---
{{% text-center %}}
## Prenez plaisir à apprendre avec des outils pédagogiques ludiques
{{% /text-center%}}
Chez Qweb.eco, nous sommes persuadés que l’apprentissage et l’intégration d’une compétence est favorisée par le jeu. Les études le prouvent : les  personnes qui  considèrent  leur  métier  comme  un  jeu réussissent  mieux  que  celles  qui  s’enferment  dans  un travail  routinier. Nos formations et coachings facilitent l'interaction et l’échange avec des mécanismes ludiques.

---
{{% text-center %}}
## Catalogue de fomations
{{% /text-center%}}
à venir prochainement...